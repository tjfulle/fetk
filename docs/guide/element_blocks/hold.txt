§3.2 ELEMENT TYPE
 EXTERNAL
INTERNAL
  Element Type Descriptor
Application Dimension Model Formulation Shape Node-Conf DOF-Conf
 Element Type Id
   MELT
 Figure 3.2.
§3.2
The element type describes its function in sufficient detail to the FE program so that it can process
the element through the appropriate modules or subroutines.
The element type is concisely defined by the user through an element type name. A name may be viewed as an external representation of the type. Legal type names are paired trough an internal table called the Master Element Library Table or MELT, with with an internal list of attributes collectively called the element type descriptor.
§3.2.1 ElementTypeName
The element type name, or simply element name, is a character string through which the user specifies
the function of the element, as well as some distinguishing characteristics.
The complexity of this name depends on the level of generality of the underlying FE code. As an extreme case, consider a simple code that uses one and only one element type. No name is then required.
Most finite element codes, however, implement several element types and names appear. Some ancient programs use numeric codes such as 103 or 410. More common nowadays is the use of character strings with some mnemonic touches, such as "QUAD9" or "BEAM2".
In the present scheme an element type name is assumed. The choice of names is up to the program developer. The name implicitly define the element type descriptor, as illustrated in Figure 3.1.
The connection of the type name in the MEDT and and the descriptor in the MELT is effected through a type index, which is a pointer to the appropriate entry of the MELT.
§3.2.2 ElementTypeDescriptor
The element type descriptor or simply element descriptor is a list that contains seven components
ELEMENT TYPE
ISOQ4 STM 2 LAMINA ISO QU1 1000 110000
Element type name and descriptor: (a) the name is external and specified by the user whereas the descriptor is internal and only seen by the program; (b) an example.
3–5

Chapter 3: INDIVIDUAL ELEMENTS 3–6 Table 3.1 Application specification in element type descriptor (examples)
 Identifier Application
 "ACF" Acoustic fluid
"EMM" Electromagnetic medium "EUF" Euler fluid
"NSF" Navier-Stokes fluid "RBM" Rigid body mechanics "STM" Structural mechanics "THM" Thermomechanical
   that collectively classify the element in sufficient detail to be routed, during the processing phases, to the appropriate element formation routines. The descriptor does not include information processed within the routine, such as node coordinates, materials and fabrication data.
The seven components are either items or sublists, and appear in the following order:
descriptor = {application, dimensionality, model, formulation, geometric-shape, node-configuration, freedom-configuration }
(3.9)
See Figure 3.2. In programming, these may be identified by names such as:
eDL = { eDapp, eDdim, eDmod, eDfrm, eDsha, eDnod, eDdof } (3.10)
or similar conventions.
As noted above, the connection between element type names and descriptor is done through a built-in data structure called the Master Element Library Table or MELT. This data structure is updated by the program developer as elements are added to the library. The organization of the MELT is described in Chapter 4.
§3.2.2.1 Application
The application item is a 3-character identification tag that explains what kind of physical problem the element is used for. The most important ones are listed in Table 3.1. The data structures described here emphasize Structural Mechanics elements, with tag "STM". However, many of the attributes apply equally to the other applications of Table 3.1, if modeled by the finite element method.
§3.2.2.2 Dimensionality
The dimensionality item is an integer that defines the number of intrinsic space dimensions of a mechanical element: 0, 1, 2 or 3. Multipoint contraint and multifreedom constraint elements are conventionally assigned a dimensionality of 0.
3–6

3–7 §3.2 ELEMENT TYPE Table 3.2 Structural Mechanics model specification in element type descriptor
 Appl Dim Identifier Mathematical model
 "STM" 0 "POINT" "STM" 0 "CON" "STM" 0 "CON" "STM" 1 "BAR" "STM" 1 "PBEAM0" "STM" 1 "PBEAM1" "STM" 1 "SBEAM0" "STM" 1 "SBEAM1" "STM" 1 "CABLE" "STM" 1 "ARCH" "STM" 1 "FRUST0" "STM" 1 "FRUST1" "STM" 2 "LAMIN" "STM" 2 "SLICE" "STM" 2 "RING" "STM" 2 "PLATE0" "STM" 2 "PLATE1" "STM" 2 "SHELL0" "STM" 2 "SHELL1" "STM" 3 "SOLID"
Point
Multipoint constraint Multifreedom constraint Bar
C0 plane beam
C1 plane beam
C0 space beam
C1 space beam
Cable
Arch
C 0 axisymmetric shell
C 1 axisymmetric shell Plane stress (membrane) Plane strain slice Axisymmetric solid
C0 (Reissner-Mindlin) plate C1 (Kirchhoff) plate
C0 shell
C1 shell
Solid
   This attribute should not be confused with spatial dimensionality, which is 2 or 3 for 2D or 3D analysis, respectively. Spatial dimensionality is the same for all elements. For example a bar or beam element has intrinsic dimensionality of 1 but may be used in a 2D or 3D context.
§3.2.2.3 Model
The model item is linked to the application and dimensionality attributes to define the mathematical model used in the element derivation. It is specified by a character string containing up to 6 characters. Some common models used in Structural Mechanics applications are alphabetically listed in Table 3.2.
In that Table, 0/1 after bending models identifies the so-called C0 and C1 formulations, respectively. For example "PLATE1" is another moniker for Kirchhoff plate or thin plate. "LAMINA" and "SLICE" are names for 2D elements in plane stress and plane strain, respectively. "FRUS0" and "FRUS1" are axisymmetric shell elements obtained by rotating C0 and C1 beams, respectively, 360 degrees. "RING" is an axisymmetric solid element.
Other mathematical model identifiers can of course be added to the list as program capabilities expand. One common expansion is through the addition of composite elements; for example a stiffened shell or a girder box.
3–7

Chapter 3: INDIVIDUAL ELEMENTS 3–8 Table 3.3 Structural Mechanics formulation specification in element type descriptor
 Identifier Formulation
 "ANS" Assumed Natural Strain
"AND" Assumed Natural Deviatoric Strain "EFF" Extended Free Formulation
"EXT" Externally supplied: used for MFCs "FRF" Free Formulation
"FLE" Flexibility (assumed force)
"HET" Heterosis
"HYB" Hybrid
"HYP" Hyperparametric
"ISO" Isoparametric
"MOM" Mechanics of Materials
"SEM" Semi-Loof
"TEM" Template (most general of all) "TMX" Transfer Matrix
   §3.2.2.4 Formulation
The formulation item indicates, through a three-letter string, the methodology used for deriving the
element. Some of the most common formulation identifiers are listed in Table 3.3.
§3.2.2.5 GeometricShape
The geometric shape, in conjunction with the intrinsic dimensionality attribute, identifies the element geometry by a 3-character string. Allowable identifiers, paired with element dimensionalities, are listed in Table 3.4.
Not much geometrical variety can be expected of course in 0 and 1 dimensions. Actually there are two possibility for zero dimension: "DOT" identifies a point element (for example, a concentrated mass or a spring-to-ground) whereas "CON" applies to MPC (MultiPoint Constraint) and MFC (MultiFreedom Constraint) elements.
In two dimensions triangles and quadrilaterals are possible, with identifiers "TR" and "QU" respec- tively, followed by a number. In three dimensions the choice is between tetrahedra, wedges and bricks, which are identified by "TE", "WE" and "BR", respectively, also followed by a number. The number following the shape identifier specifies whether the element is constructed as a unit, or as a macroelement assembly.
The "ARB" identifier is intended as a catch-all for shapes that are too complicated to be described by one word.
3–8

3–9 §3.2 ELEMENT TYPE Table 3.4 Shape specification in element type descriptor
 Dim Identifier Geometric shape
 0 "DOT"
0 "CON"
1 "SEG"
2 "TR1"
2 "QU1" 2 "QU2"
2 "QU4"
3 "TE1"
3 "WE1" 3 "WE3" 3 "BR1" 3 "BR5" 3 "BR6"
2-3 "VOR" 1-3 "ARB"
Point; e.g. a concentrated mass Constraint element (conventionally) Segment
Triangle
Quadrilateral
Quadrilateral built of 2 triangles Quadrilateral built of 4 triangles Tetrahedron
Wedge
Wedge built of 3 tetrahedra
Brick
Brick built of 5 tetrahedra
Brick built of 6 tetrahedra
Voronoi cell
Arbitrary shape: defined by other attributes
   §3.2.2.6 ElementNodalConfiguration
The node configuration of the element is specified by a list of four integer items:
node-configuration := {eCNX, eSNX, eFNX, eINX } (3.11)
Here eCNX, eSNX, eFNX and eINX, are abbreviations for element-corner-node-index, element-side- node-index, element-face-node-index and element-internal-node-index, respectively. Their value ranges from 0 through 2 with the meaning explained in Table 3.5. For storage and display convenience, the four indices are often decimally packed as one integer:
eCSFIX = 1000*eCNX + 100*eSNX + 10*eFNX + eINX (3.12)
Note that the full set of indices only applies to elements with intrinsic dimensionality of 3. If dimensionality is 2 or less, eINX is ignored. If dimensionality is 1 or less, eFNX and eINX are ignored. Finally if dimensionality is 0, all except eCNX are ignored. Some specific examples:
20-node triquadratic brick
27-node triquadratic brick
64-node tricubic brick
8-node serendipity quad
9-node biquadratic quad
10-node cubic lamina (plane stress) triangle
ECSFI=1100 ECSFI=1111 ECSFI=1222 ECSFI=1100 ECSFI=1110 ECSFI=1210
(64=8*1+12*2+6*4+8) (10=3*1+3*2+1)
3–9

Chapter 3: INDIVIDUAL ELEMENTS 3–10 Table 3.5 Node configuration in element type descriptor
 Dim Indices Meaning/remarks
 1–3 eCNX=0,1 1–3 eSNX=0,1,2 2–3 eFNX=0,1,2
3 eFNX=0,1,2 0 eCSFIX=1000
element has eCNX nodes per corner element has eSNX nodes per side
if eFNX=0, no face nodes
if eFNX=1, one node per face
if eFNX=2, as many face nodes as face corners if eINX=0, no face nodes
if eINX=1, one node per face
if eINX=2, as many face nodes as face corners all nodes are treated as corners
 Some exotic node configurations are omitted; cf. Remark 3.2
   REMARK 3.2
Note that the meaning of ”interior nodes” here does not agree with the usual FEM definition except for 3D elements. A plane stress element with one center node has eFNX=1 and eINX=0 instead of eFNX=0 and eINX=1. In other words, that center node is considered a face node. Similarly a bar or beam element with two nodes at its third-points has eSNX=2, eFNX=eINX=0; that is, those nodes are considered side nodes. As explained next, the reason for this convention is mixability.
Why use indices instead of actual node counts? Mixability tests (connecting different elements together) are considerably simplified using this item. More specifically, elements with same application, model, formulation, eCNX, eSNX, eFNX, (in 3D) and eCNX, eSNX (in 2D) can be mixed if, in addition, some geometric compatibility conditions are met, while dimensionality and geometric shape attributes may be different. [Note that eINX and eFNX are irrelevant in 3D and 2D, respectively.] Two examples:
(i) Two solid elements with ECSFI=1220, 1221 and 1222 are candidates for mixing because interior nodes are irrelevant to such mating. This is true regardless of whether those elements are bricks, wedges or tetrahedra. Of course the common faces must have the same geometric shape; this represents another part of the mixability test.
(ii) Mixability across different dimensionalities is often of interest. For example, a 3-node bar with ECSFI=1100 attached along the edge of a quadrilateral or triangular lamina with ECSFI=1100 or 1110. Similarly a 2-node beam, which has ECSFI=1000, may be attached to a plate with ECSFI=1000 but not to one with ECSFI=1100.
Values other than those listed in Table 3.5 are reserved for exotic configurations. For example, elements with ”double corner nodes” (eCNX=2) are occassionally useful to represent singularities in fracture mechanics.
§3.2.2.7 ElementFreedomConfiguration
The element freedom configuration defines the default freedom assignation at element nodes. Here “default” means that the assignation may be ovewriden on a node by node basis by the Freedom Definition data studied in Chapter 7. A brief introduction to the concept of freedom assignment is needed here. More details are given in that chapter. The discussion below is limited to Structural Mechanics elements.
3–10

3–11 §3.3 ELEMENT NODE LIST Table 3.6 Freedom Signatures in element descriptor
At each node n a local Cartesian reference system (x ̄ , y ̄ , z ̄ ) may be used to define freedom direc- nnn
tions. See Figure 2.1. If these directions are not explicitly specified, they are assumed to coincide with the global system (x, y, z). (This is in fact the case in the present implementation).
In principle up to six degrees of freedom can be assigned at a node. These are identified by the symbols
tx, ty, tz, rx, ry, rz (3.13) Symbols tx, ty and tz denote translations along axes x ̄ , y ̄ and z ̄ , respectively, whereas rx, ry
 Signature Meaning
 0 Freedom is not assigned (“off”)
1 Freedom is assigned (“on”)
   and rz denotes the rotations about those axes. If a particular freedom, say rz, appears in the finite element equations of the element it is said to be assigned. Otherwise it is unassigned. For example, consider a flat thin plate element located in the global (x, y) plane, with all local reference systems aligned with that system. Then tz, rx, and ry are assigned whereas tx, ty and rz are not.
The freedom configuration at corner, side, face and interior nodes of an element is defined by four integers denoted by
eFS = { eFSC, eFSS, eFSF, eFSI } (3.14) which are called element freedom signatures. Omitted values are assumed zero.
Each of the integers in (3.14) is formed by decimally packing six individual freedom signature values:
eFSC = 100000*txC + 10000*tyC + 1000*tzC + 100*rxC + 10*ryC + rzC eFSS = 100000*txS + 10000*tyS + 1000*tzS + 100*rxS + 10*ryC + rzS eFSF = 100000*txF + 10000*tyF + 1000*tzF + 100*rxF + 10*ryC + rzF eFSI = 100000*txI + 10000*tyI + 1000*tzI + 100*rxI + 10*ryC + rzI
The freedom signature txC is associated with the translation tx at corner nodes. Similarly, rxS is associated with the rotation rx at side nodes. And so on. The signature value specifies whether the associated freedom is on or off, as indicated in Table 3.6. Signatures other than 0 or 1 are used in the Master Freedom Definition Table described in Chapter 7 to incorporate the attribute called freedom activity.
REMARK 3.3
If the element lacks node of a certain type the corresponding signature is zero. For example, if the element has only corner nodes, eFSS = eFSF = eFSI = 0, and these may be omitted from the list (3.14).
REMARK 3.4
For an MFC element the signatures, if given, are conventionally set to zero, since the node configuration is obtained from other information.
3–11
nnn
(3.15)

Chapter 3: INDIVIDUAL ELEMENTS 3–12 §3.3 ELEMENT NODE LIST
The list of node numbers of the element is supplied by the user. This information is grouped into four sublists, at least one of which is non empty:
nodelist = { cornernodes, sidenodes, facenodes, internalnodes } (3.16)
The first list, cornernodes, lists the corner nodes of the element by external node number. If the element has only corner nodes all other lists are empty. If the element has side nodes, they are listed in sidenodes, and so on.
In programming the following variable names are often used for these lists:
eXNL = { eFXNL, eSXNL, eFXNL, eILNL } (3.17)
in which the letter X emphasizes that these are external node numbers. If the X is dropped, as in eNL, it implies that internal node numbers appear.
Corner nodes must be ordered according to the conventions applicable to each geometric element shape. All corner node numbers must be nonzero and positive.
Negative or zero node numbers are acceptable for the last three lists. A zero number identifies a ”hierarchically constrained” node or H-node used in certain transition elements; such freedoms do not appear in the assembled equations. A negative number identifies an unconnected node or U-node, whose degrees of freedom are not connected to those of another element. In linear static analysis those freedoms may be statically condensed at the element level. I-nodes are always U-nodes.
 Figure 3.3.
EXAMPLE 3.1
9 26 41
Example element to illustrate the configuration of the element node list eXNL.
7 24 39
8
40
25
The element nodelist of the 9-node quadrilateral element shown in Figure 3.3 following the usual counterclockwise-numbering conventions of most FEM codes, is
eXNL = { {7,9,41, 39}, {8,26,40,24 }, {25 }} (3.18)
REMARK 3.5
There are 2D “flux elements” that contain no corner nodes. They are connected through midside nodes in 2D and face nodes in 3D. The specification of their node list is a bit unusual in that the first list is empty in 2D whereas the first two lists are empty in 3D. Such elements are not commonly used in Structural Mechanics, however.
3–12

3–13 §3.5 INDIVIDUAL ELEMENT DEFINITION LIST §3.4 ELEMENT CODES
The element codes are integers that indirectly specify individual element properties by pointing to constitutive and fabrication tables. Up to three codes may be listed:
element-codes = {constitutive, fabrication, template } (3.19) The variable names often used for these items are
eCL = {cc, fc, tc } or {ccod, fcod, tcod } (3.20) Of these the first two codes must appear explicitly, although values of zero are acceptable to indicate
a “null” or void pointer. The last one is optional.
§3.4.1 ConstitutiveCode
This is a pointer to the Constitutive tables that specify material properties. The format of these tables
is presented in Chapter 4.
Some elements (for example, constraint elements) may not require constitutive properties, in which case the code is zero.
§3.4.2 FabricationCode
This is a pointer to the Fabrication tables that specify fabrication properties. These properties include geometric information such as thicknesses or areas. For constraint elements fabrication tables are used to specify coefficients in multipoint or multifreedom constraints. The format of these tables is presented in Chapter 5.
Some elements (for example, solid elements) may not require fabrication properties, in which case the code is zero.
§3.4.3 TemplateCode
This is a pointer to the Template tables that specify numerical coefficients applicable to the construc- tion of certain classes of elements that fall under the TEM Formulation. If the element does not pertain to that class, this code can be left out or set to zero.
EXAMPLE 3.2
If the constitutive code is 3 and the fabrication code is 4, the element code list is
eCL = {3,4 } (3.21)
3–13

Chapter 3: INDIVIDUAL ELEMENTS
3–14
Individual Element Definition List
stored in MEDT
via MELT
stored in MEDT
stored in MEDT
Figure 3.4.
InternalNodes
Configuration of the Individual Element Definition List.
   Element Identifier
Object Name
External Number
         Element Type Descriptor
Application
Dimension
Model
Formulation
Geometric Shape
Nodal Configuration
Freedom Configutation
      Element Nodes
CornerNodes
SideNodes
FaceNodes
     Element Codes
Constitution
Fabrication
Template
 §3.5
The Individual Element Definition List or eDL defines the properties of an individual element This
eDL is a list of four components that have been previously described in §3.1 through §3.4:
eDL := { identifier, type, nodes, codes } (3.22)
The variable names often used for these items are
eDL = { eleident, eletype, elenodes, elecodes } (3.23)
As described above, each of the eDT components is a list, or a list of lists. The complete eDL configuration is summarized in Figure 3.4.
The eDLs of all elements are stacked to built the Master Element Definition Table described in Chapter 4.
INDIVIDUAL ELEMENT DEFINITION LIST
3–14
