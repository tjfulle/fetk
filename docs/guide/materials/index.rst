.. _mat_index:

=========
Materials
=========

Each element's constitutive response is governed by a material model.

.. rubric:: Contents

.. toctree::
   :maxdepth: 1

   defining
   models
   internal
   input
