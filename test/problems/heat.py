import os
import numpy as np
import fetk.problems.heat as heat
from fetk.util.filesystem import working_dir


def series_solution(x, q0, k, N=100):
    def fun(n):
        an = 0.5 * (2.0 * n - 1.0) * np.pi
        top = (-1) ** n * np.cos(an * x[:, 1]) * np.cosh(an * x[:, 0])
        bot = an ** 3 * np.cosh(an)
        return top / bot

    a = (1 - x[:, 1] ** 2) + 4.0 * np.sum([fun(n) for n in range(1, N)], 0)
    return q0 / 2 / k * a


def test_heat_1(tmpdir, datadir):
    """Same as test_plane_1, but with an input file"""
    with working_dir(tmpdir):
        model = heat.run_file(os.path.join(datadir, "heat1.yaml"))
        analytic = series_solution(model.coord, 1.0, 1.0)
        temp = model.solution.u.flatten()
        ix = np.where(analytic > 1e-12)
        assert np.allclose(analytic[ix], temp[ix], atol=2e-2)


def test_heat_2(tmpdir, datadir):
    """Same as test_plane_1, but with an input file"""
    with working_dir(tmpdir):
        model = heat.run_file(os.path.join(datadir, "heat2.yaml"))
        analytic = series_solution(model.coord, 1.0, 1.0)
        ix = np.where(analytic > 1e-12)
        temp = model.solution.u.flatten()
        assert np.allclose(analytic[ix], temp[ix], atol=3.0e-2)


def test_heat_3(tmpdir, datadir):
    """Same as test_plane_1, but with an input file"""
    with working_dir(tmpdir):
        model = heat.run_file(os.path.join(datadir, "heat3.yaml"))
        analytic = series_solution(model.coord, 1.0, 1.0)
        ix = np.where(analytic > 1e-12)
        temp = model.solution.u.flatten()
        assert np.allclose(analytic[ix], temp[ix], atol=5.0e-2)
