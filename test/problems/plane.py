import os
import numpy as np
import fetk.problems.plane as plane
from fetk.util.filesystem import working_dir


def test_plane_1(tmpdir, datadir):
    """Same as test_plane_1, but with an input file"""
    with working_dir(tmpdir):
        model = plane.run_file(os.path.join(datadir, "plane1.yaml"))
        expected_u = np.array(
            [
                [-0.165141e-5, -0.12504538e-4],
                [0.165141e-5, -0.12504535e-4],
                [0.0, 0.0],
                [0.0, 0.0],
                [0.0, -0.16279491e-4],
            ]
        )
        assert np.allclose(model.solution.u, expected_u)


if __name__ == "__main__":
    test_plane_1(os.getcwd())
