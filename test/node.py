import numpy as np
import fetk.node


def test_factory():
    nodes = [[1, 0, 0, 0], [4, 1, 1, 1], [20, 2, 2, 2]]
    coords, node_map = fetk.node.factory(nodes)
    assert np.allclose(
        coords, np.array([[0.0, 0.0, 0.0], [1.0, 1.0, 1.0], [2.0, 2.0, 2.0]])
    )
    assert node_map == {1: 0, 4: 1, 20: 2}
