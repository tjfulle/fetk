import numpy as np
import fetk.element as el
from fetk.material import elastic


link_properties = {"area": 1.0}
link_material = elastic({"E": 1.0})


def test_1d_link():
    coords = np.array([0.0, 1.0])
    element = el.L1D2(**link_properties)
    ke = element.stiffness(coords, link_material)
    expected = np.array([[1.0, -1.0], [-1.0, 1.0]])
    assert np.allclose(ke, expected), "Element stiffness does not match expected"


def test_2d_link():
    coords = np.array([[0.0, 0.0], [1.0, 0.0]])
    element = el.L2D2(**link_properties)
    ke = element.stiffness(coords, link_material)
    expected = np.array(
        [
            [1.0, 0.0, -1.0, 0.0],
            [0.0, 0.0, 0.0, 0.0],
            [-1.0, 0.0, 1.0, 0.0],
            [0.0, 0.0, 0.0, 0.0],
        ]
    )
    assert np.allclose(ke, expected), "Element stiffness does not match expected"


def test_3d_link():
    coords = np.array([[0.0, 0.0, 0.0], [1.0, 0.0, 0.0]])
    element = el.L3D2(**link_properties)
    ke = element.stiffness(coords, link_material)
    expected = np.array(
        [
            [1.0, 0.0, 0.0, -1.0, 0.0, 0.0],
            [0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
            [0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
            [-1.0, 0.0, 0.0, 1.0, 0.0, 0.0],
            [0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
            [0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
        ]
    )
    assert np.allclose(ke, expected), "Element stiffness does not match expected"
