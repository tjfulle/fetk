import numpy as np

# Maximum number of dofs.  dofs are ordered:
# 1: ux (displacement x)
# 2: uy (displacement y)
# 3: uz (displacement z)
# 4: tx (rotation x)
# 5: ty (rotation y)
# 6: tz (rotation z)
# 7: T (temperature)
max_dof = 7
ux, uy, uz, tx, ty, tz, T = range(max_dof)

neumann, dirichlet = 0, 1
types = (neumann, dirichlet)


class dof_manager:
    """Container for prescribed degrees of freedom (boundary conditions)

    Notes
    -----
    The DOF manager manages degrees of freedom through the following arrays:

    doftags : ndarray
        Boundary condition tags
        doftags[d, 0] is the external node ID of the dth boundary condition
        doftags[d, i+1] is the constaint type for dof dof i
          (0 for Neumann, 1 for Dirichlet)
    dofvals : ndarray
        Boundary condition values
        dofvals[d, i] is the magnitude of the constraint corresponding to
          doftags[d, i+1]

    """

    def __init__(self, num_node):
        self._tags = np.zeros((num_node, max_dof), dtype=int)
        self._values = np.zeros((num_node, max_dof))

    def __len__(self):
        return self._tags.shape[0]

    def __getitem__(self, node_no):
        return self.get(node_no)

    def tags(self, active_dofs):
        return self._tags[:, active_dofs]

    def values(self, active_dofs):
        return self._values[:, active_dofs]

    def set(self, type, node_no, dofs, magnitude):
        if isinstance(dofs, int):
            dofs = [dofs]
        for dof in dofs:
            self._tags[node_no, dof] = type
            self._values[node_no, dof] = magnitude

    @property
    def active_dofs(self):
        return self._active_dofs

    @active_dofs.setter
    def active_dofs(self, arg):
        self._active_dofs = np.asarray(arg)


def apply(K, F, doftags, dofvals):
    """Apply boundary conditions

    Parameters
    ----------
    K : ndarray
        Global stiffness
    F : ndarray
        Global force
    doftags : ndarray
        Degree of freedom tags. doftags[i,j]=1 if the jth degree of freedom of
        node i has a known (prescribed) displacement, otherwise the node has a
        known force and doftags[i,j]=0.
    dofvals : ndarray
        Degree of freedom values.  dofvals[i,j] is the magnitude of the dof
        cooresponding to the prescribed condition indicated by doftags[i,j].

    Returns
    -------
    Kbc : ndarray
        Global stiffness with boundary conditions applied
    Fbc : ndarray
        Global force with boundary conditions applied

    """
    num_nodes, num_dof_per_node = doftags.shape
    num_dofs = num_nodes * num_dof_per_node

    Kbc = K.copy()
    Q = force(doftags, dofvals)
    Fbc = F + Q

    # Dirichlet boundary conditions
    for i in range(num_nodes):
        for j in range(num_dof_per_node):
            if doftags[i, j] == dirichlet:
                I = i * num_dof_per_node + j
                Fbc -= [K[k, I] * dofvals[i, j] for k in range(num_dofs)]
                Kbc[I, :] = Kbc[:, I] = 0
                Kbc[I, I] = 1

    # Further modify RHS for Dirichlet boundary
    # This must be done after the loop above.
    for i in range(num_nodes):
        for j in range(num_dof_per_node):
            if doftags[i, j] == dirichlet:
                I = i * num_dof_per_node + j
                Fbc[I] = dofvals[i, j]

    return Kbc, Fbc


def force(doftags, dofvals):
    """Force contribution on Neumann boundary"""
    num_nodes, num_dof_per_node = doftags.shape
    num_dofs = num_nodes * num_dof_per_node
    Q = np.zeros(num_dofs, dtype=float)
    for i in range(num_nodes):
        for j in range(num_dof_per_node):
            I = num_dof_per_node * i + j
            Q[I] = dofvals[i, j] if doftags[i, j] == neumann else 0.0
    return Q
